#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

#define NUM_LEDS 16

#define BRIGHTNESS 10

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);

uint8_t  offset = 0; // Position of spinny eyes
uint32_t color  = 0xFF0000; // Start red

void setup() {
  pixels.begin();
  pixels.setBrightness(BRIGHTNESS); // 1/3 brightness
}

void loop() {

    for(int i=0; i<16; i++) {
      uint32_t c = 0;
      if(((offset + i) & 7) < 1) c = 0xFF00FF;
      else c = 0xFFFFFF;
      if(((offset + i + 1) & 7) < 1) c = 0x0000FF;
      
      if(((offset + i + 2) & 7) < 1) c = 0x00FF00;
      if(((offset + i + 3) & 7) < 1) c = 0xFFFF00;
      if(((offset + i + 4) & 7) < 1) c = 0x964B00;
      if(((offset + i + 5) & 7) < 1) c = color;
      
      //if(((offset + i + i) & 7) < 2) c = 0xFFFF00;
      //else c = 0xFFFFFF;
      pixels.setPixelColor( i, c); // First eye
    }
    //pixels.setPixelColor( 18, 150,0,150); // First eye
    pixels.show();
    offset++;
    delay(80);
}
